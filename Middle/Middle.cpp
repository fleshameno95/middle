﻿#include <iostream>
#include <stdexcept>

class Fraction
{
public:
    Fraction(int n, int d) : numerator(n), denominator(d)
    {
        if (denominator == 0)
        {
            throw std::runtime_error("Denominator cannot be zero");
        }
    }

    int getNumerator() const { return numerator; }

    int getDenominator() const { return denominator; }

private:
    int numerator;
    int denominator;
};

int main()
{
    int numerator, denominator;
    try
    {
        std::cout << "Enter numerator: ";
        std::cin >> numerator;
        std::cout << "Enter denominator: ";
        std::cin >> denominator;

        Fraction fraction(numerator, denominator);

        std::cout << "Fraction: " << fraction.getNumerator() << "/" << fraction.getDenominator() << std::endl;
    }
    catch (std::exception& e)
    {
        std::cout << "Invalid input: " << e.what() << std::endl;
    }

    return 0;
}